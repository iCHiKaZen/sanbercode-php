<?php

require_once('animal.php');
require ('ape.php');
require('frog.php');
$sheep = new Animal("Shaun");

echo "Name : " . $sheep->name . "<br>"; 
echo "legs : " . $sheep->legs . "<BR>"; 
echo "cold - blooded : " . $sheep->cold_blooded . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; 
echo "legs : " . $sungokong->legs . "<BR>"; 
echo "cold - blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();
echo "<br><br>";
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; 
echo "legs : " . $kodok->legs . "<BR>"; 
echo "cold - blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump() ;

?>